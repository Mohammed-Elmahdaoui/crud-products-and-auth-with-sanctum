<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $products = Product::query();
        if ($request->has('name')) {
            $products->where('name', 'like', '%'.$request->name.'%');
        }
        return $products->get();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['string', 'required'],
            'slug' => ['string', 'required'],
            'price' => ['required'],
            'description' => ['string', 'nullable'],
        ]);

        return Product::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => ['string'],
            'slug' => ['string'],
//           'price'=>['decimal'],
            'description' => ['string'],
        ]);

        $product->update($request->all());

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        return $product->delete();
    }

    /**
     * search the specified resource from storage.
     */
    public function search(string $name)
    {
        return Product::where('name', 'like', `%$name%`)->get();
    }
}
